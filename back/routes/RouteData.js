var express = require('express');
var router = express.Router();
const { json } = require('body-parser');
var url = require('url');
require('dotenv').config();
const { hotel } = require("../models/hotel");
const { media } = require("../models/media");
const serviceData = require('../services/serviceData');
const { log } = require('console');
const { siteTouristique } = require('../models/siteTouristique');
const querystring = require('querystring');

// INSERT Hotel //
router.post('/insertHotel', function (req, res, next) {
    (async () => {
        try {
            console.log(req.body);
            var hotel1 = new hotel(
                req.body.id,
                req.body.idVille,
                req.body.nom,
                req.body.description,
                req.body.contact,
                req.body.mail,
                req.body.siteWeb,
                req.body.nombreChambre,
                req.body.prixParHeure,
                req.body.geom,
                req.body.etat);
            const response = {
                Status: 200,
                Message: "Insertion OK",
                Resultat: await serviceData.insertHotel(hotel1)
            };
            res.status(200).json(response);
        } catch (err) {
            res.status(500).json(err.message);
        }
    })()
});

// INSERT media //
router.post('/insertMedia', function (req, res, next) {
    (async () => {
        try {
            console.log(req.body);
            var media1 = new media(
                req.body.id,
                req.body.type1,
                req.body.type2,
                req.body.designation,
                req.body.lien,
                req.body.etat);
            const response = {
                Status: 200,
                Message: "Insertion OK",
                Resultat: await serviceData.insertMedia(media1)
            };
            res.status(200).json(response);
        } catch (err) {
            console.log(err);
            res.status(500).json(err.message);
        }
    })()
});

// INSERT siteTouristique //
router.post('/insertSiteTouristique', function (req, res, next) {
    (async () => {
        try {
            console.log(req.body);
            var siteT1 = new siteTouristique(
                req.body.id,
                req.body.idVille,
                req.body.nom,
                req.body.description,
                req.body.contact,
                req.body.mail,
                req.body.siteWeb,
                req.body.prixDeVisite,
                req.body.ouvert,
                req.body.fermer,
                req.body.geom,
                req.body.etat);
            const response = {
                Status: 200,
                Message: "Insertion OK",
                Resultat: await serviceData.insertSiteTouristique(siteT1)
            };
            res.status(200).json(response);
        } catch (err) {
            console.log(err.message);
            res.status(500).json(err.message);
        }
    })()
});

// Reserver hotel //
router.post('/reserver', function (req, res, next) {
    (async () => {
        try {
            console.log(req.body);

            const response = {
                Status: 200,
                Message: "Insertion OK",
                Resultat: await serviceData.insertSiteTouristique(siteT1)
            };
            res.status(200).json(response);
        } catch (err) {
            console.log(err.message);
            res.status(500).json(err.message);
        }
    })()
});

// detail hotel //
router.get('/hotelDetail', function (req, res, next) {
    (async () => {
        try {
            const response = {
                Status: 200,
                Message: "Detail Hotel",
                Resultat: await serviceData.getHotelDetail(req.body.id)
            };
            res.status(200).json(response);
        } catch (err) {
            console.log(err);
            res.status(500).json(err.message);
        }
    })()
});

// detail siteTouristique //
router.get('/siteTouristiqueDetail', function (req, res, next) {
    (async () => {
        try {
            const response = {
                Status: 200,
                Message: "Detail Site Touristique",
                Resultat: await serviceData.getSiteTouristiqueDetail(req.body.id)
            };
            res.status(200).json(response);
        } catch (err) {
            console.log(err);
            res.status(500).json(err.message);
        }
    })()
});

// recherche //
router.get('/recherche', function (req, res, next) {
    (async () => {
        try {
            const response = {
                Status: 200,
                Message: "Recherche de " + req.query.motCle,
                Resultat: await serviceData.recherche(req.query.motCle)
            };
            res.status(200).json(response);
        } catch (err) {
            console.log(err);
            res.status(500).json(err.message);
        }
    })()
});

module.exports = router;