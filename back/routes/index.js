var express = require('express');
var router = express.Router();
const dbpg = require("../middlewares/connectionDB")
var users = require("../models/users")
const utilitaire = require("../services/utilitaire");
const zone = require("../models/zone");

/* GET home page. */
router.get('/', function(req, res, next) {
 (async()=>{
  try {
    //insert zone
    res.render('index', { title: 'Express' });
    const client = await dbpg.connect();
    console.log((await client.query('select count(*) from users')).rows[0].count);
    await client.release();
  } catch (error) {
    console.error(error);
  }
 })()
});

module.exports = router;
