require('dotenv').config();
const dbpg = require('../middlewares/connectionDB');
const utilitaire = require("../services/utilitaire");

//Model media
class siteTouristique {
    id;
    idVille;
    nom;
    description;
    contact;
    mail;
    siteWeb;
    prixDeVisite;
    ouvert;
    fermer;
    geom;
    etat;

    constructor(id,idVille,nom,description,contact,mail,siteWeb,prixDeVisite,ouvert,fermer,geom,etat) {
      this.id = id;
      this.idVille = idVille;
      this.nom = nom;
      this.description = description;
      this.contact = contact;
      this.mail = mail;
      this.siteWeb = siteWeb;
      this.prixDeVisite = prixDeVisite;
      this.ouvert = ouvert;
      this.fermer = fermer;
      this.geom = geom;
      this.etat = etat;
    }

    async insert(){
        const clientdb = await dbpg.connect()
        var resultat = [];
        try {
            await clientdb.query('BEGIN')
            //generation d'id
            var idZone = "ST";
            idZone = idZone+utilitaire.genIdNumber((await clientdb.query("SELECT nextval('seqsitetouristique')")).rows[0].nextval);
            this.id = idZone;
            console.log("Insert in base: "+this.id);
            //insertion 
            const insertText = "INSERT INTO sitetouristique VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)";
            const insertVal = [this.id,
                            this.idVille,
                            this.nom,
                            this.description,
                            this.contact,
                            this.mail,
                            this.siteWeb,
                            this.prixDeVisite,
                            this.ouvert,
                            this.fermer,
                            this.geom,
                            process.env.ETAT_CREER];
            await clientdb.query(insertText,insertVal);
            await clientdb.query('COMMIT')
          } catch (e) {
            await clientdb.query('ROLLBACK')
            throw e
          } finally {
            await clientdb.release()
          }
          return resultat;
    }
}

exports.siteTouristique = siteTouristique;