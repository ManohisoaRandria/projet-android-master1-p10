require('dotenv').config();
const dbpg = require('../middlewares/connectionDB');
const utilitaire = require("../services/utilitaire");

//Model users
class users {
  id;
  nom;
  prenom;
  mail;
  motDePasse;
  contact;
  etat;

  constructor(id, nom, prenom, mail, motdepasse, contact, etat) {
    this.id = id;
    this.prenom = prenom;
    this.nom = nom;
    this.mail = mail;
    this.motDePasse = motdepasse;
    this.contact = contact;
    this.etat = etat;
  }

  async insert() {
    const clientdb = await dbpg.connect()
    var resultat = [];
    try {
      await clientdb.query('BEGIN')
      //generation d'id
      var iduser = "US";
      let res = await clientdb.query("SELECT nextval('sequsers')");
      //insertion 
      iduser = iduser + utilitaire.genIdNumber(res.rows[0].nextval)
      this.id = iduser;
      const insertText = "INSERT INTO users VALUES ($1,$2,$3,$4,$5,$6,$7)";
      const insertVal = [this.id,
      this.nom,
      this.prenom,
      this.mail,
      this.motDePasse,
      this.contact,
      this.etat];
      resultat = clientdb.query(insertText, insertVal);
      await clientdb.query('COMMIT')
    } catch (e) {
      await clientdb.query('ROLLBACK')
      throw e
    } finally {
      await clientdb.release()
    }
    return resultat;
  }
}

module.exports = users;