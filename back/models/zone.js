require('dotenv').config();
const dbpg = require('../middlewares/connectionDB');
const utilitaire = require("../services/utilitaire")

//Model users
class zone {
    id;
    nom;
    etat;

    constructor(id, nom,etat) {
      this.id = id;
      this.nom = nom;
      this.etat = etat;
    }

    async insert(){
        const clientdb = await dbpg.connect()
        var resultat = [];
        try {
            await clientdb.query('BEGIN')
            //generation d'id
            var idZone = "ZO";
            await clientdb.query("SELECT nextval('seqzone')",(err, res)=>{
                if(!err){
                  idZone = idZone+utilitaire.genIdNumber(res.rows[0].nextval)
                  this.id = idZone;
                  //insertion 
                    const insertText = "INSERT INTO zone VALUES ($1, $2, $3)";
                    const insertVal = [this.id,this.nom,this.etat];
                    clientdb.query(insertText,insertVal,(err, res)=>{
                        if(!err){
                            console.log("INSERT OK")
                            resultat = res;
                        }else{
                            throw new Error("ERREUR INSERT :"+err.message);
                        }
                    });
                }else{
                  throw new Error("Erreur de génération d'id");
                }
              });
            await clientdb.query('COMMIT')
          } catch (e) {
            await clientdb.query('ROLLBACK')
            throw e
          } finally {
            await clientdb.release()
          }
          return resultat;
    }
}

module.exports = zone;
