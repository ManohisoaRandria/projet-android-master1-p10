require('dotenv').config();
const dbpg = require('../middlewares/connectionDB');
const utilitaire = require("../services/utilitaire");

//Model users
class hotel {
    id;
    idVille;
    nom;
    description;
    contact;
    mail;
    siteWeb;
    nombreChambre;
    prixParHeure;
    geom;
    etat;

    constructor(id, idVille,nom,description,contact,mail,siteweb,nombrechambre,prixparheure,geom,etat) {
      this.id = id;
      this.idVille = idVille;
      this.contact = contact;
      this.nom = nom;
      this.mail = mail;
      this.description = description;
      this.siteWeb = siteweb;
      this.nombreChambre = nombrechambre;
      this.prixParHeure = prixparheure;
      this.geom = geom;
      this.etat = etat;
    }

    async insert(){
        const clientdb = await dbpg.connect()
        var resultat = [];  
        try {
            await clientdb.query('BEGIN')
            //generation d'id
            var idZone = "HO";
            idZone = idZone+utilitaire.genIdNumber((await clientdb.query("SELECT nextval('seqhotel')")).rows[0].nextval);
            this.id = idZone;
            //insertion 
            console.log("Insert in base...");
            const insertText = "INSERT INTO hotel VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)";
            const insertVal = [this.id,
                                this.idVille,
                                this.nom,
                                this.description,
                                this.contact,
                                this.mail,
                                this.siteWeb,
                                this.nombreChambre,
                                this.prixParHeure,
                                this.geom,
                                process.env.ETAT_CREER];
            resultat = await clientdb.query(insertText,insertVal);
            await clientdb.query('COMMIT')
          } catch (e) {
            await clientdb.query('ROLLBACK')
            throw new Error(e);
          } finally {
            await clientdb.release()
          }
          return resultat;
    }

}

//Model reservation
class reservation {
    id;
    idHotel;
    idUsers;
    nombreChambre;
    dateReservation;
    dateHeurDeb;
    dateHeureFin;
    montant;
    etat;

    constructor(id, idHotel,idUsers,numchambre,datereservation,dateheurdeb,dateheurefin,montant,etat) {
      this.id = id;
      this.idUsers = idUsers;
      this.idHotel = idHotel;
      this.numChambre = numchambre;
      this.dateReservation = datereservation;
      this.dateHeurDeb = dateheurdeb;
      this.dateHeureFin = dateheurefin;
      this.montant = montant;
      this.etat = etat;
    }

    async insert(){
        const clientdb = await dbpg.connect()
        var resultat = [];
        try {
            await clientdb.query('BEGIN')
            //generation d'id
            var idZone = "RE";
            idZone = idZone+utilitaire.genIdNumber(await clientdb.query("SELECT nextval('seqreservation')").rows[0].nextval);
            //insertion 
            const insertText = "INSERT INTO reservation VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9)";
            const insertVal = [this.id,
                                this.idHotel,
                                this.idUsers,
                                this.numChambre,
                                this.dateReservation,
                                this.dateHeurDeb,
                                this.dateheurefin,
                                this.montant,
                                this.etat];
            resultat = await clientdb.query(insertText,insertVal);
            await clientdb.query('COMMIT')
          } catch (e) {
            await clientdb.query('ROLLBACK')
            throw e
          } finally {
            await clientdb.release()
          }
          return resultat;
    }
}

exports.hotel = hotel;
exports.reservation = reservation;