require('dotenv').config();
const dbpg = require('../middlewares/connectionDB');
const utilitaire = require("../services/utilitaire");

//Model users
class ville {
    id;
    idZone;
    nom;
    geom;
    dim;
    etat;

    constructor(id, idZone,nom,geom,dim,etat) {
      this.id = id;
      this.nom = nom;
      this.idZone = idZone;
      this.geom = geom;
      this.dim = dim;
      this.etat = etat;
    }

    async insert(){
        const clientdb = await dbpg.connect()
        var resultat = [];
        try {
            await clientdb.query('BEGIN')
            //generation d'id
            var idZone = "VL";
            await clientdb.query("SELECT nextval('seqville')",(err, res)=>{
                if(!err){
                  idZone = idZone+utilitaire.genIdNumber(res.rows[0].nextval)
                  this.id = idZone;
                  //insertion 
                    const insertText = "INSERT INTO ville VALUES ($1,$2,$3,$4,$5,$6,$7)";
                    const insertVal = [this.id,
                                        this.idzone,
                                        this.nom,
                                        this.geom,
                                        this.dim,
                                        this.etat];
                    clientdb.query(insertText,insertVal,(err, res)=>{
                        if(!err){
                            console.log("INSERT OK")
                            resultat = res;
                        }else{
                            throw new Error("ERREUR INSERT :"+err.message);
                        }
                    });
                }else{
                  throw new Error("Erreur de génération d'id");
                }
              });
            await clientdb.query('COMMIT')
          } catch (e) {
            await clientdb.query('ROLLBACK')
            throw e
          } finally {
            await clientdb.release()
          }
          return resultat;
    }
}

module.exports = ville;