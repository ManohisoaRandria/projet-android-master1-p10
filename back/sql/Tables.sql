/*-------*/
CREATE EXTENSION unaccent

/*----USER---*/
CREATE SEQUENCE seqUsers START 1;
CREATE TABLE Users (
    id VARCHAR(10) PRIMARY KEY,
    nom VARCHAR(60),
    prenom VARCHAR(60),
    mail VARCHAR(60) UNIQUE NOT NULL,
    motDePasse VARCHAR(60) NOT NULL,
    contact VARCHAR(15),
    etat NUMERIC
);

/*----ZONE----*/
CREATE SEQUENCE seqZone START 1;
CREATE TABLE Zone (
    id VARCHAR(10) PRIMARY KEY,
    nom VARCHAR(10) UNIQUE NOT NULL,
    etat NUMERIC
);

/*---VILLE---*/
CREATE SEQUENCE seqVille START 1;
CREATE TABLE Ville (
    id VARCHAR(10) PRIMARY KEY,
    idZone VARCHAR(60) REFERENCES Zone(id),
    nom VARCHAR(60) UNIQUE NOT NULL,
    geom GEOMETRY,
    dim GEOMETRY,
    etat NUMERIC
);

/*---SITE TOURISTIQUE----*/
CREATE SEQUENCE seqSiteTouristique START 1;
CREATE TABLE SiteTouristique (
    id VARCHAR(10) PRIMARY KEY,
    idVille VARCHAR(10) REFERENCES Ville(id),
    nom VARCHAR(60) UNIQUE NOT NULL,
    desciption TEXT,
    contact VARCHAR(13),
    mail VARCHAR(60),
    siteWeb VARCHAR(60),
    prixDeVisite DECIMAL(10,2),
    ouvert TIME,
    fermer TIME, 
    geom GEOMETRY,
    etat NUMERIC
);


/*-----HOTEL-----*/
CREATE SEQUENCE seqHotel START 1;
CREATE TABLE Hotel (
    id VARCHAR(10) PRIMARY KEY,
    idVille VARCHAR(10) REFERENCES Ville(id),
    nom VARCHAR(60) NOT NULL,
    desciption TEXT,
    contact VARCHAR(13),
    mail VARCHAR(60),
    siteWeb VARCHAR(60),
    nombreChambre NUMERIC,
    prixParHeure DECIMAL(10,2),
    geom GEOMETRY,
    etat NUMERIC 
);

/*-----RESERVATION-----*/
CREATE SEQUENCE seqReservation START 1;
CREATE TABLE Reservation (
    id VARCHAR(10) PRIMARY KEY,
    idHotel VARCHAR(10) REFERENCES Hotel(id),
    idUsers VARCHAR(10) REFERENCES Users(id),
    NumChambre NUMERIC,
    dateReservation TIMESTAMP,
    dateHeureDeb TIMESTAMP,
    dateHeureFin TIMESTAMP,
    montant DECIMAL(10,2),
    Etat NUMERIC
);

/*------MEDIA-------*/
CREATE SEQUENCE seqMedia START 1;
CREATE TABLE Media (
    id VARCHAR(10) PRIMARY KEY,
    type1 VARCHAR(10) NOT NULL, /*IMAGE,VIDEO...*/
    type2 VARCHAR(10) NOT NULL, /*Hotel, Site...*/
    designation VARCHAR(60),
    lien TEXT NOT NULL,
    etat NUMERIC
);


/*-zone
-site
-media
-hotel
-reservation
-user*/

-- ALTER SEQUENCE seqzone RESTART WITH 3

/*----DATA INIT------*/
--users
INSERT INTO Users VALUES ('US000000'||nextval('sequsers'),'RAMANANTSEHENO','Tahianarivelo','ramamnantsehenotahiana@gmail.com','1234','+261340467224',0);

--zone
INSERT INTO zone VALUES ('ZO000000'||nextval('seqzone'),'Nord',0);
INSERT INTO zone VALUES ('ZO000000'||nextval('seqzone'),'Sud',0);
INSERT INTO zone VALUES ('ZO000000'||nextval('seqzone'),'Est',0);
INSERT INTO zone VALUES ('ZO000000'||nextval('seqzone'),'Ouest',0);
INSERT INTO zone VALUES ('ZO000000'||nextval('seqzone'),'Centre',0);           

--ville
INSERT INTO ville VALUES ('VL000000'||nextval('seqville'),'ZO0000005','Antananarivo',null,null,0);
INSERT INTO ville VALUES ('VL000000'||nextval('seqville'),'ZO0000001','Antsiranana',null,null,0);
INSERT INTO ville VALUES ('VL000000'||nextval('seqville'),'ZO0000002','Toliara',null,null,0);

--hotel
INSERT INTO hotel VALUES('HO000000'||nextval('seqhotel'),'VL0000001','Restaurant La Ribaudière','la direction et le personnel agreables et competents. La chambre spacieuse, confortable et propre… manque juste un refrigerateur. Hotel bien situe au centre ville, la restauration excellente, et les plats tres bien presentes','+261345286445','','',125,20000,null,0);
INSERT INTO hotel VALUES('HO000000'||nextval('seqhotel'),'VL0000001','Sole Hotel','Les chambres comprennent une television par satellite, un bureau, un coffre-fort et du linge de lit. La salle de bains est pourvue d''une douche et d''articles de toilette gratuits','+261345796828','','',125,20000,null,0);

--site
INSERT INTO media VALUES('MD'||nextval('seqhotel'),'Site','VL0000003',null,'https://scontent.ftnr5-1.fna.fbcdn.net/v/t1.6435-9/93272853_515945475757044_1413291660565020672_n.jpg?_nc_cat=111&cb=99be929b-59f725be&ccb=1-7&_nc_sid=8bfeb9&_nc_eui2=AeElRWdgBhk-Y2HWwFotAikgOLAbFhLvfKA4sBsWEu98oFfN7hlx_htPrg0KW-D4dxK5uqZC0PIVfMZ9kjAMmA74&_nc_ohc=GMmNUiScQGcAX8TeEuh&_nc_ht=scontent.ftnr5-1.fna&oh=00_AfCMuH5B0bomDFaAUqKDpsvzQM50NEF6SLPaXJSBpthwwg&oe=64F74F1A');
INSERT INTO media VALUES('MD'||nextval('seqhotel'),'Site','ST0000003',null,'https://web.archive.org/web/20161024213033if_/http://static.panoramio.com/photos/large/82277926.jpg');
INSERT INTO media VALUES('MD'||nextval('seqhotel'),'Hotel','HO0000001',null,'https://cf.bstatic.com/xdata/images/hotel/max1024x768/270290700.jpg?k=7f07c62bed89f11cee86af8b6d4cf05710e8aaa8437710ad5b8491972bf07d21&o=&hp=1');
INSERT INTO media VALUES('MD'||nextval('seqhotel'),'Hotel','HO0000002',null,'https://cf.bstatic.com/xdata/images/hotel/max1280x900/264991418.jpg?k=6998919eb4bcde746517edf388782064bbf8603444bd3285bda64a44f1d17619&o=&hp=1');



-- view
CREATE OR REPLACE VIEW hotelMedia as 
select hotel.*,media.lien 
from hotel left join media on hotel.id = media.type2;

CREATE OR REPLACE VIEW siteMedia as 
select sitetouristique.*,media.lien 
from sitetouristique left join media on sitetouristique.id = media.type2;



CREATE OR REPLACE VIEW hotelDetail as select  
    hotelMedia.id,
    hotelMedia.idVille,
    hotelMedia.nom ,
    hotelMedia.desciption ,
    hotelMedia.contact,
    hotelMedia.mail ,
    hotelMedia.siteWeb ,
    hotelMedia.nombreChambre ,
    hotelMedia.prixParHeure ,
    ST_AsGeoJSON(hotelMedia.geom) geom,
    hotelMedia.lien,
    hotelMedia.etat  ,
        ville.nom as nomVille,
        zone.nom as nomZone,
        CONCAT('hotel',' ',hotelMedia.nom,' ',ville.nom,' ',zone.nom) as recherche
from hotelMedia,ville,zone where hotelMedia.idville = ville.id and ville.idZone = zone.id;

CREATE OR REPLACE VIEW sitetouristiqueDetail as select  
    siteMedia.id ,
    siteMedia.idVille ,
    siteMedia.nom ,
    siteMedia.desciption ,
    siteMedia.contact ,
    siteMedia.mail ,
    siteMedia.siteWeb ,
    siteMedia.prixDeVisite ,
    siteMedia.ouvert ,
    siteMedia.fermer , 
    ST_AsGeoJSON(siteMedia.geom) geom ,
    siteMedia.lien,
    siteMedia.etat ,
        ville.nom as nomVille,
        zone.nom as nomZone,
        CONCAT('site touristique',' ',siteMedia.nom,' ',ville.nom,' ',zone.nom) as recherche
from siteMedia,ville,zone where siteMedia.idville = ville.id and ville.idZone = zone.id;