require('dotenv').config();
const users = require('../models/users');
const dbpg = require('../middlewares/connectionDB');


// //Insert users
const insertUsers = async (id,nom,prenom,mail,motdepasse,contact,etat) => {
    const clientdb = await dbpg.connect();
    try{
        await clientdb.query('BEGIN')
        //verification mail
        console.log("Verification mail");
        var count = (await clientdb.query("select count(*) from users where mail = $1",[mail])).rows[0].count;
        if(count >= 1){
            throw new Error("Ce mail existe déjà.");
        }
        const user = new users(id,nom, prenom,mail, motdepasse,contact,process.env.ETAT_CREER);
        await user.insert();
        await clientdb.query('COMMIT');
    }catch(error){
        await clientdb.query('ROLLBACK');
        console.log(error);
        throw error
    } finally {
        await clientdb.release();
    }
}

exports.insertUsers = insertUsers;