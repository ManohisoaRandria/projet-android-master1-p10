require('dotenv').config();
const { hotel } = require('../models/hotel');
const dbpg = require('../middlewares/connectionDB');
const { connect } = require('mongoose');
const { request } = require('express');

////////INSERT////////////////
const insertHotel = async (hotel1) => {
    const clientdb = await dbpg.connect();
    try {
        await clientdb.query('BEGIN')
        //verification mail&c
        var count = (await clientdb.query("select count(*) from hotel where mail = $1", [hotel1.mail])).rows[0].count;
        if (count >= 1) {
            throw new Error("Ce mail existe déjà.");
        }
        await hotel1.insert();
        await clientdb.query('COMMIT');
    } catch (error) {
        await clientdb.query('ROLLBACK');
        throw new Error(error);
    } finally {
        await clientdb.release();
    }
}

const insertVille = async (id, idZone, nom, geom, dim, etat) => {
    const clientdb = await dbpg.connect();
    try {
        await clientdb.query('BEGIN')
        //verification mail
        await clientdb.query("select count(*) from hotel where nom = $1", [nom], (err, res) => {
            if (!err) {
                if (res.rows[0].count >= 1) {
                    throw new Error("Ce nom de ville existe déjà.");
                }
            }
        });
        const ville = new ville(id, idZone, nom, geom, dim, process.env.ETAT_CREER);
        await ville.insert().then((res) => { }).catch((err) => {
            throw err;
        });
        await clientdb.query('COMMIT');
    } catch (error) {
        await clientdb.query('ROLLBACK');
        throw error
    } finally {
        await clientdb.release();
    }
}

const insertMedia = async (media1) => {
    const clientdb = await dbpg.connect();
    try {
        await clientdb.query('BEGIN')
        await media1.insert();
        await clientdb.query('COMMIT');
    } catch (error) {
        await clientdb.query('ROLLBACK');
        throw new Error(error);
    } finally {
        await clientdb.release();
    }
}

const insertSiteTouristique = async (siteT1) => {
    const clientdb = await dbpg.connect();
    try {
        await clientdb.query('BEGIN')
        //verification mail
        var count = (await clientdb.query("select count(*) from sitetouristique where mail = $1", [siteT1.mail])).rows[0].count;
        if (count >= 1) {
            throw new Error("Ce mail existe déjà.");
        }
        await siteT1.insert();
        await clientdb.query('COMMIT');
    } catch (error) {
        await clientdb.query('ROLLBACK');
        throw new Error(error);
    } finally {
        await clientdb.release();
    }
}

const reserverHotel = async (idUser, idHotel, numChambre, dateHeurDeb, dateheurefin) => {
    const clientdb = await dbpg.connect();
    try {
        await clientdb.query('BEGIN')
        //verification reservation dispo
        await clientdb.query('COMMIT');
    } catch (error) {
        await clientdb.query('ROLLBACK');
        throw error
    } finally {
        await clientdb.release();
    }
}


/////// SELECT /////////////
const recherche = async (motCle) => {
    const clientdb = await dbpg.connect();
    var reponse = {
        siteTouristique: null,
        hotel: null
    };
    try {
        await clientdb.query('BEGIN')
        console.log(motCle);
        reponse.hotel = (await clientdb.query("select * from hotelDetail where unaccent(lower(recherche)) LIKE unaccent(lower('%" + motCle + "%'))")).rows;
        reponse.siteTouristique = (await clientdb.query("select * from sitetouristiqueDetail where unaccent(lower(recherche)) LIKE unaccent(lower('%" + motCle + "%'))")).rows;
        for (let i = 0; i < reponse.hotel.length; i++) {
            let geom = JSON.parse(reponse.hotel[i].geom);
            let lat = geom.coordinates[0]
            let lng = geom.coordinates[1]
            reponse.hotel[i] = { ...reponse.hotel[i], lat: lat, lng: lng }
        }
        for (let i = 0; i < reponse.siteTouristique.length; i++) {
            let geom = JSON.parse(reponse.siteTouristique[i].geom);
            let lat = geom.coordinates[0]
            let lng = geom.coordinates[1]
            reponse.siteTouristique[i] = { ...reponse.siteTouristique[i], lat: lat, lng: lng }
        }
        await clientdb.query('COMMIT');
    } catch (error) {
        await clientdb.query('ROLLBACK');
        throw error
    } finally {
        await clientdb.release();
    }
    return reponse;
}

const getHotelDetail = async (id) => {
    const clientdb = await dbpg.connect();
    var reponse = {
        detail: null,
        media: null,
    };
    try {
        await clientdb.query('BEGIN')
        console.log(id);
        reponse.detail = (await clientdb.query("select * from hotelDetail where id = '" + id + "'")).rows;
        for (let i = 0; i < reponse.detail.length; i++) {
            let geom = JSON.parse(reponse.detail[i].geom);
            let lat = geom.coordinates[0]
            let lng = geom.coordinates[1]
            reponse.detail[i] = { ...reponse.detail[i], lat: lat, lng: lng }
        }
        reponse.media = (await clientdb.query("select * from media where type2 = '" + id + "'")).rows;
        await clientdb.query('COMMIT');
    } catch (error) {
        await clientdb.query('ROLLBACK');
        throw error
    } finally {
        await clientdb.release();
    }
    return reponse;
}

const getSiteTouristiqueDetail = async (id) => {
    const clientdb = await dbpg.connect();
    var reponse = {
        detail: null,
        media: null,
    };
    try {
        await clientdb.query('BEGIN')
        console.log(id);
        reponse.detail = (await clientdb.query("select * from siteTouristiqueDetail where id = '" + id + "'")).rows;
        for (let i = 0; i < reponse.detail.length; i++) {
            let geom = JSON.parse(reponse.detail[i].geom);
            let lat = geom.coordinates[0]
            let lng = geom.coordinates[1]
            reponse.detail[i] = { ...reponse.detail[i], lat: lat, lng: lng }
        }
        reponse.media = (await clientdb.query("select * from media where type2 = '" + id + "'")).rows;
        await clientdb.query('COMMIT');
    } catch (error) {
        await clientdb.query('ROLLBACK');
        throw error
    } finally {
        await clientdb.release();
    }
    return reponse;
}

exports.insertVille = insertVille;
exports.insertHotel = insertHotel;
exports.insertMedia = insertMedia;
exports.insertSiteTouristique = insertSiteTouristique;
exports.recherche = recherche;
exports.getHotelDetail = getHotelDetail;
exports.getSiteTouristiqueDetail = getSiteTouristiqueDetail;