require('dotenv').config();

const genIdNumber = (number) => {
    let totalLength = process.env.SIZE_MAX_GEN;
    let numberStr = number.toString();
  if (numberStr.length >= totalLength) {
    return numberStr;
  } else {
    let zerosToAdd = totalLength - numberStr.length;
    return "0".repeat(zerosToAdd) + numberStr;
  }
}

exports.genIdNumber = genIdNumber;