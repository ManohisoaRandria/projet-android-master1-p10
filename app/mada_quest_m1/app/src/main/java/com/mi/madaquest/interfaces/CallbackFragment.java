package com.mi.madaquest.interfaces;

import android.content.Context;

import androidx.fragment.app.Fragment;

public interface CallbackFragment {
    void changeFragment(Fragment fragment);
    Context getContext();
    void goToMainHome();
}
