package com.mi.madaquest.vue.activites;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.mi.madaquest.R;

public class AccueilActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ///Pour masquer l'action bar
        getSupportActionBar().hide();

        setContentView(R.layout.activity_accueil);
    }
}