package com.mi.madaquest.model;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class CheckNetwork {

    private CheckNetwork() {
       //
    }

    /**
     * Pour vérifier si la connexion internet marche
     * @param mContext
     * @return
     */
    public static Boolean checknetwork(Context mContext) {

        NetworkInfo info = ((ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        if (info == null || !info.isConnected()) {
            return false;
        }
        // here is the roaming option, you can change it if you want to
        // disable internet while roaming, just return false
        info.isRoaming();

        return true;

    }
}
