package com.mi.madaquest.vue.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.mapbox.geojson.Point;
import com.mapbox.maps.CameraOptions;
import com.mapbox.maps.MapView;
import com.mapbox.maps.MapboxMap;
import com.mapbox.maps.Style;
import com.mapbox.maps.plugin.annotation.AnnotationConfig;
import com.mapbox.maps.plugin.annotation.AnnotationPlugin;
import com.mapbox.maps.plugin.annotation.AnnotationPluginImplKt;
import com.mapbox.maps.plugin.annotation.generated.OnPointAnnotationClickListener;
import com.mapbox.maps.plugin.annotation.generated.PointAnnotation;
import com.mapbox.maps.plugin.annotation.generated.PointAnnotationManager;
import com.mapbox.maps.plugin.annotation.generated.PointAnnotationManagerKt;
import com.mapbox.maps.plugin.annotation.generated.PointAnnotationOptions;
import com.mi.madaquest.R;
import com.mi.madaquest.interfaces.CallbackFragment;
import com.mi.madaquest.model.Constantes;
import com.mi.madaquest.model.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class MapFragment extends Fragment {

    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private View rootView;

    ProgressDialog progressDialog;
    PointAnnotationManager pointAnnotationManager;

    CallbackFragment callbackFragment;
    MapView mapView;
    MapboxMap mapboxMap;
    Bitmap markerBitmap;
    Button searchBtn;
    EditText searchField;
    JSONArray listHotel = new JSONArray();
    JSONArray listSite = new JSONArray();
    public MapFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_map, container, false);
        markerBitmap = bitmapFromDrawableRes(
                requireContext(),
                R.drawable.red_marker1
        );
        mapView = rootView.findViewById(R.id.mapView);
        searchBtn=rootView.findViewById(R.id.search_btn);
        searchField=rootView.findViewById(R.id.search_field);

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search(searchField.getText().toString().trim());
            }
        });
        progressDialog=new ProgressDialog(requireContext());
        progressDialog.setMessage("Veuillez patienter svp...");
        progressDialog.setCanceledOnTouchOutside(false);

        mapboxMap = mapView.getMapboxMap();
        mapboxMap.loadStyleUri(Style.MAPBOX_STREETS,new Style.OnStyleLoaded() {
            @Override
            public void onStyleLoaded(Style style) {
                initAnnotationManager();
                centerMapOnLocation( 46.91254,	-18.8791902);
                search("");
            }
        });
        return rootView;
    }
    private void search(String moCLe){
        progressDialog.show();
        String url = Constantes.API_URL+"/data/recherche?motCle="+moCLe.trim();

        // creating a new variable for our request queue
        MySingleton singleton = MySingleton.getInstance(requireContext());
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url,null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            pointAnnotationManager.deleteAll();

                            //mparse anle donnee avy any am back
                            listHotel = new JSONArray();
                            listSite = new JSONArray();

                            JSONObject res=response.getJSONObject("Resultat");
                            JSONArray site=res.getJSONArray("siteTouristique");
                            JSONArray hotel=res.getJSONArray("hotel");
                            listSite = site;
                            listHotel = hotel;
                            for (int i=0;i<site.length();i++){
                                addAnnotationToMap( site.getJSONObject(i).getDouble("lng"),   site.getJSONObject(i).getDouble("lat"),site.getJSONObject(i).getString("id"),site.getJSONObject(i).getString("nom"),"Site");
                            }
                            for (int i=0;i<hotel.length();i++){
                                addAnnotationToMap( hotel.getJSONObject(i).getDouble("lng"),   hotel.getJSONObject(i).getDouble("lat"),hotel.getJSONObject(i).getString("id"),hotel.getJSONObject(i).getString("nom"),"Hotel");
                            }
                            progressDialog.dismiss();

                            //eto mamono anle clavier refa avy manao recherche
                            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(searchField.getWindowToken(), 0);
                            centerMapOnLocation( 46.91254,	-18.8791902);
                        } catch (JSONException e) {
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(requireContext(), "Erreur lors du datafetch", Toast.LENGTH_SHORT).show();
            }
        });
        singleton.addToRequestQueue(req);
    }
    private void centerMapOnLocation(double longitude, double latitude) {
        CameraOptions camOpt= new CameraOptions.Builder()
                .center(Point.fromLngLat(longitude, latitude))
                .pitch(0.0)
                .zoom(4.97)
                .bearing(7.0)
                .build();

        mapboxMap.setCamera(camOpt);
    }
    private void initAnnotationManager(){
        AnnotationPlugin annotationAPI=AnnotationPluginImplKt.getAnnotations(mapView);
        pointAnnotationManager =  PointAnnotationManagerKt.createPointAnnotationManager(annotationAPI,new AnnotationConfig());
        pointAnnotationManager.addClickListener(new OnPointAnnotationClickListener() {
            @Override
            public boolean onAnnotationClick(@NonNull PointAnnotation annotation) {
//                Toast.makeText(requireContext(),annotation.getTextField()+":"+annotation.getTextHaloColorString(), Toast.LENGTH_SHORT).show();
                try {
                    //mifidra detail
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                    //Mitady an ilay site na hotel affichena ny détail
                    Bundle bundle = new Bundle();
                    boolean trouver = false;
                    //Mitady hotel
                    if (trouver == false) {
                        for (int i = 0; i < listHotel.length(); i++) {
                            if (trouver) {
                                break;
                            }
                            if (listHotel.getJSONObject(i).getString("id") == annotation.getTextHaloColorString()) {
                                bundle.putString("type","Hotel");
                                bundle.putString("id",listHotel.getJSONObject(i).getString("id"));
                                bundle.putString("nom",listHotel.getJSONObject(i).getString("nom"));
                                bundle.putString("description",listHotel.getJSONObject(i).getString("desciption"));
                                bundle.putString("lienImage",listHotel.getJSONObject(i).getString("id"));
                                bundle.putString("contact",listHotel.getJSONObject(i).getString("contact"));
                                bundle.putString("mail",listHotel.getJSONObject(i).getString("mail"));
                                bundle.putString("siteWeb",listHotel.getJSONObject(i).getString("siteweb"));
                                bundle.putString("nombrechambre",listHotel.getJSONObject(i).getString("nombrechambre"));
                                bundle.putString("prix",listHotel.getJSONObject(i).getString("prixparheure"));
                                bundle.putString("nomville",listHotel.getJSONObject(i).getString("nomville"));
                                bundle.putString("nomzone",listHotel.getJSONObject(i).getString("nomzone"));
                                bundle.putString("lien",listHotel.getJSONObject(i).getString("lien"));
                                System.out.println("P1: "+listHotel.getJSONObject(i).getString("lien"));
                                trouver = true;
                            }
                        }
                    }
                    //Mitady site
                    if (trouver == false) {
                        for (int i = 0; i < listSite.length(); i++) {
                            if (trouver) {
                                break;
                            }
                            if (listSite.getJSONObject(i).getString("id") == annotation.getTextHaloColorString()) {
                                bundle.putString("type","Site");
                                bundle.putString("id",listSite.getJSONObject(i).getString("id"));
                                bundle.putString("nom",listSite.getJSONObject(i).getString("nom"));
                                bundle.putString("description",listSite.getJSONObject(i).getString("desciption"));
                                bundle.putString("lienImage",listSite.getJSONObject(i).getString("id"));
                                bundle.putString("ouvert",listSite.getJSONObject(i).getString("ouvert"));
                                bundle.putString("fermer",listSite.getJSONObject(i).getString("fermer"));
                                bundle.putString("contact",listSite.getJSONObject(i).getString("contact"));
                                bundle.putString("mail",listSite.getJSONObject(i).getString("mail"));
                                bundle.putString("siteWeb",listSite.getJSONObject(i).getString("siteweb"));
                                bundle.putString("prix",listSite.getJSONObject(i).getString("prixdevisite"));
                                bundle.putString("nomville",listSite.getJSONObject(i).getString("nomville"));
                                bundle.putString("nomzone",listSite.getJSONObject(i).getString("nomzone"));
                                bundle.putString("lien",listSite.getJSONObject(i).getString("lien"));
                                trouver = true;
                            }
                        }
                    }

                    DetailSiteFragment detailSiteFragment = new DetailSiteFragment();
                    //ity ilay mpasser an ilay info makany @ fragment detail
                    detailSiteFragment.setArguments(bundle);
                    fragmentTransaction.replace(R.id.fragment_container, detailSiteFragment);
                    fragmentTransaction.commit();

                }
                catch(JSONException e){
                    System.out.print(e.getMessage());
                }

                return true;
            }
        });
    }
    //ty no manampy marker amle map
    private void addAnnotationToMap(double longitude, double latitude,String idHotelSite,String nomHotelSite,String type) {

        if (markerBitmap != null) {
            if(type.equals("Site")){
                markerBitmap = bitmapFromDrawableRes(
                        requireContext(),
                        R.drawable.red_markerb
                );
            }else{
                markerBitmap = bitmapFromDrawableRes(
                        requireContext(),
                        R.drawable.red_marker1
                );
            }
            PointAnnotationOptions pointAnnotationOptions = new PointAnnotationOptions()
                    .withPoint(Point.fromLngLat(longitude,latitude))
                    .withIconImage(markerBitmap)
                    .withIconSize(1.0f); // You might need to adjust the icon size
            pointAnnotationOptions.setTextField(nomHotelSite);
            pointAnnotationOptions.setTextHaloColor(idHotelSite);
            pointAnnotationOptions.setTextLineHeight(1.0);
            pointAnnotationManager.create(pointAnnotationOptions);
        }
    }
    private void createPoint(){

    }

    private Bitmap bitmapFromDrawableRes(Context context, @DrawableRes int resourceId) {
        return convertDrawableToBitmap(AppCompatResources.getDrawable(context, resourceId));
    }

    private Bitmap convertDrawableToBitmap(Drawable sourceDrawable) {
        if (sourceDrawable == null) {
            return null;
        }
        if (sourceDrawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) sourceDrawable).getBitmap();
        } else {
            Drawable drawable = sourceDrawable.getConstantState().newDrawable().mutate();
            Bitmap bitmap = Bitmap.createBitmap(
                    drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(),
                    Bitmap.Config.ARGB_8888
            );
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        }
    }


}