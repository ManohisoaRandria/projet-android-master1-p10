package com.mi.madaquest.model;

public class Constantes {

    /**
     * Cette classe ne peut pas être instancié
     */
    private Constantes() {

    }

    public final static String REGEX_EMAIL="^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";

    public final static String API_URL="https://projet-android-master1-p10.onrender.com";

    public final static int RC_SIGN_IN = 101;

    public final static String ID_NOTIFICATION = "NOTIF_04";

    public final static String GAMES_LIST = "ListOfGames_1.0";

    public final static String PREF_GAMES_LIST = "ListOfPrefsGames";

    public final static String SETTINGS_LIST = "ListOfSettings";

    public static final String SHARED_PREFS = "shared_prefs";


}
