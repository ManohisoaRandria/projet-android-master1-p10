package com.mi.madaquest.model;

public class User {
    private String email;
    private String pseudo;

    public User() {}

    public User(String email, String pseudo) {
        this.email = email;
        this.pseudo = pseudo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }
}
