package com.mi.madaquest.vue.fragments;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;

import com.mi.madaquest.R;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class DetailSiteFragment extends Fragment {

    private static RecyclerView.Adapter adapter;
    static View.OnClickListener myOnClickListener;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private View rootView;
    private URL urlImage = null;
    ProgressDialog progressDialog;
    private ImageView image;
    public DetailSiteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppCompatActivity activity = (AppCompatActivity) getActivity();

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_detail_site, container, false);

        //eto no maka an'ilay argument
        Bundle bundle = getArguments();
        TextView nomDetail = (TextView) rootView.findViewById(R.id.nom_detail);
        TextView contactDetail = (TextView) rootView.findViewById(R.id.contact_detail);
        TextView mailDetail = (TextView) rootView.findViewById(R.id.mail_detail);
        TextView siteWebDetail = (TextView) rootView.findViewById(R.id.siteweb_detail);
        TextView descriptionDetail = (TextView) rootView.findViewById(R.id.description_detail);
        TextView prixDetail = (TextView) rootView.findViewById(R.id.prix_detail);
        nomDetail.setText(bundle.getString("nom"));
        contactDetail.setText(bundle.getString("contact"));
        mailDetail.setText(bundle.getString("mail"));
        siteWebDetail.setText(bundle.getString("siteweb"));
        descriptionDetail.setText(bundle.getString("description"));
        prixDetail.setText(bundle.getString("prix") + " Ar");

        progressDialog=new ProgressDialog(requireContext());
        progressDialog.setMessage("Veuillez patienter svp...");
        progressDialog.setCanceledOnTouchOutside(false);



        if(bundle.getString("type") == "Site"){
            if (activity != null) {
                activity.getSupportActionBar().setTitle("Site touristique");
            }
            Button btn = rootView.findViewById(R.id.btnReserver);
            btn.setVisibility(View.INVISIBLE);
        }else{
            if (activity != null) {
                activity.getSupportActionBar().setTitle("Hôtel");
            }
            System.out.println("etoooo");
            rootView.findViewById(R.id.btnReserver).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("atooo");
                    //ty le manao anle notification
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(requireContext(), "idChan")
                            .setSmallIcon(R.drawable.red_marker)
                            .setContentTitle("Bonjour!")
                            .setContentText("Hotele reservé avec succès")
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT);

                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(requireContext());

                    // notificationId is a unique int for each notification that you must define
                    notificationManager.notify(123, builder.build());
                }
            });
        }

        image = (ImageView) rootView.findViewById(R.id.imageDetail);
        if(bundle.getString("lien") != new String("")){
            System.out.println(bundle.getString("lien"));

            progressDialog.show();
            Picasso.with(this.getContext()).load(bundle.getString("lien")).into(image);
            progressDialog.dismiss();
        }

        //downloadImage();
        return rootView;
    }



    private void downloadImage() {
        Bitmap bitmap = null;
        try {
            HttpURLConnection connection = (HttpURLConnection) urlImage.openConnection();
            InputStream inputStream = connection.getInputStream();
            bitmap = BitmapFactory.decodeStream(inputStream);
            image.setImageBitmap(bitmap);
//            progressDialog.dismiss();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}