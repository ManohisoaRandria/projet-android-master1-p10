package com.mi.madaquest.vue.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mi.madaquest.R;
import com.mi.madaquest.interfaces.CallbackFragment;
import com.mi.madaquest.model.Constantes;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class LoginFragment extends Fragment {

    CallbackFragment callbackFragment;
    TextView createNewAccount;
    TextView forgotPassword;

    EditText inputLoginEmail;
    EditText inputLoginPassword;
    Button loginBtn;
    ProgressDialog progressDialog;

    FirebaseAuth mAuth;
    FirebaseUser mUser;

    ImageView btnLoginTwitter;
    ImageView btnLoginFacebook;
    ImageView btnLoginGoogle;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);

        this.forgotPassword = rootView.findViewById(R.id.forgotPassword);
        this.inputLoginEmail = rootView.findViewById(R.id.inputLoginEmail);
        this.inputLoginPassword = rootView.findViewById(R.id.inputLoginPassword);
        this.loginBtn = rootView.findViewById(R.id.loginBtn);
        this.createNewAccount = rootView.findViewById(R.id.createNewAccount);
        this.btnLoginTwitter = rootView.findViewById(R.id.btnLoginTwitter);
        this.btnLoginFacebook = rootView.findViewById(R.id.btnLoginFacebook);
        this.btnLoginGoogle = rootView.findViewById(R.id.btnLoginGoogle);

        progressDialog=new ProgressDialog(rootView.getContext());
        mAuth=FirebaseAuth.getInstance();
        mUser=mAuth.getCurrentUser();

        this.createNewAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(callbackFragment != null) {
                    callbackFragment.changeFragment(new RegisterFragment());
                }
            }
        });

        this.forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(callbackFragment != null) {
//                    callbackFragment.changeFragment(new ForgotPasswordFragment());
//                }
                Toast.makeText(callbackFragment.getContext(), "Fonctionalité en cours de dev", Toast.LENGTH_SHORT).show();
            }
        });

        this.loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PerformAuth();
            }
        });

        this.btnLoginGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(callbackFragment.getContext(), GoogleSignInActivity.class);
//                startActivity(intent);
                Toast.makeText(callbackFragment.getContext(), "Fonctionalité en cours de dev", Toast.LENGTH_SHORT).show();
            }
        });

        this.btnLoginFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(callbackFragment.getContext(), FacebookAuthActivity.class);
//                //intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                startActivity(intent);
                Toast.makeText(callbackFragment.getContext(), "Fonctionalité en cours de dev", Toast.LENGTH_SHORT).show();
            }
        });

        this.btnLoginTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(callbackFragment.getContext(), TwitterActivity.class);
//                startActivity(intent);
                Toast.makeText(callbackFragment.getContext(), "Fonctionalité en cours de dev", Toast.LENGTH_SHORT).show();
            }
        });

        return rootView;
    }

    public void setCallbackFragment(CallbackFragment callbackFragment) {
        this.callbackFragment = callbackFragment;
    }

    private void PerformAuth() {
        String email = inputLoginEmail.getText().toString();
        String password = inputLoginPassword.getText().toString();

        if (!email.matches(Constantes.REGEX_EMAIL)) {
            inputLoginEmail.setError("Please enter a valid Email");
        } else if (password.isEmpty() || password.length() < 6) {
            inputLoginPassword.setError("Please enter a proper Password (6 characters min)");
        } else {
            progressDialog.setMessage("Veuillez patienter...");
            progressDialog.setTitle("Login");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

            mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    progressDialog.dismiss();
                    if (task.isSuccessful()) {;
                            callbackFragment.goToMainHome();
//                        }
                    } else {
                        Toast.makeText(callbackFragment.getContext(), "" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}