package com.mi.madaquest.vue.activites;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.mi.madaquest.R;
import com.mi.madaquest.model.Constantes;
import com.mi.madaquest.vue.fragments.AboutFragment;
import com.mi.madaquest.vue.fragments.DetailSiteFragment;
import com.mi.madaquest.vue.fragments.SettingFragment;
import com.mi.madaquest.vue.fragments.MapFragment;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private TextView nav_username;
    private TextView nav_email;
    private ImageView nav_imageProfil;
    private Toolbar toolbar;
    ProgressDialog progressDialog;
    SharedPreferences sharedpreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Home");
        setSupportActionBar(toolbar);
        createNotificationChannel();

        drawerLayout = findViewById(R.id.draw_layout);
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_draw_open, R.string.navigation_draw_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        ///To change text of a TextView in navigation drawer header
        View headerView = navigationView.getHeaderView(0);
        this.nav_username = headerView.findViewById(R.id.nav_username);
        this.nav_email = headerView.findViewById(R.id.nav_email);
        this.nav_imageProfil = headerView.findViewById(R.id.nav_imageProfil);
        progressDialog=new ProgressDialog(MainActivity.this);
        sharedpreferences = getSharedPreferences(Constantes.SHARED_PREFS, (MainActivity.this).MODE_PRIVATE);

        this.nav_email.setText(FirebaseAuth.getInstance().getCurrentUser().getEmail());
        this.nav_username.setText( FirebaseAuth.getInstance().getCurrentUser().getDisplayName());

        getSupportFragmentManager().beginTransaction().replace((R.id.fragment_container), new MapFragment()).commit();
        toolbar.setTitle("Accueil");
    }

    private void setPseudoProfil(String pseudo) {
        //Log.d("Pseudo", pseudo);
        nav_username.setText(pseudo);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("PseudoUser", pseudo);
        editor.apply();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            if (f instanceof DetailSiteFragment) {
                // do operations
                getSupportFragmentManager().beginTransaction().replace((R.id.fragment_container), new MapFragment()).commit();
                toolbar.setTitle("Accueil");
            }else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                getSupportFragmentManager().beginTransaction().replace((R.id.fragment_container), new MapFragment()).commit();
                toolbar.setTitle("Accueil");
                break;
            case R.id.nav_about:
                getSupportFragmentManager().beginTransaction().replace((R.id.fragment_container), new AboutFragment()).commit();
                toolbar.setTitle("A propos");
                break;
            case R.id.nav_settings:
                getPreferences();
                break;
            case R.id.nav_logout:
                ///Clear sharedPreference before signout
                SharedPreferences sharedpreferences = getSharedPreferences(Constantes.SHARED_PREFS, (MainActivity.this).MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.clear();
                editor.apply();

                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(MainActivity.this, AuthActivity.class);
                //Ferme l'activity en cours avant d'en ouvrir une autre
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                //Call this to finish the current activity
                finish();
                break;

        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name ="idChan";
            String description ="idChan";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("idChan", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
    private void getPreferences() {
        progressDialog.setMessage("Please Wait...");
        progressDialog.setTitle("Fetching Data");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
//        FirebaseDatabase.getInstance(Constantes.DATABASE_URL).getReference("GamesPrefs")
//                .child(FirebaseAuth.getInstance().getCurrentUser().getUid()).get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
//            @Override
//            public void onComplete(@NonNull Task<DataSnapshot> task) {
//                if (!task.isSuccessful()) {
//                    Log.d("MMMMMM1", "Error getting data", task.getException());
//                    Toast.makeText(MainActivity.this, "An Error Occurred, Try Again Later Or Check Your Network.", Toast.LENGTH_SHORT).show();
//                    progressDialog.dismiss();
//                } else {
//                    ArrayList<CollectionSettings> prefGames = new ArrayList<>();
//                    ArrayList<CollectionSettings> prefOther = new ArrayList<>();
//                    ArrayList<Games> data = (ArrayList<Games>) Serializer.deSerialize(Constantes.GAMES_LIST, MainActivity.this);
//                    ///Donc l'utilisateur n'a pas encore de préférences sauvegardé
//                    if (task.getResult().getValue() == null) {
//
//                        for (int i = 0; i < data.size(); i++) {
//                            prefGames.add(new CollectionSettings(data.get(i).getId(), data.get(i).getName(), "GamesPrefs", true));
//                        }
//                        prefOther.add(new CollectionSettings(Constantes.ID_NOTIFICATION, "Notification", "GamesPrefs", true));
//                        Log.d("MMMMMM2", "No Data from db");
//                    } else {
//                        ///get All prefs for games
//                        for (int i = 0; i < data.size(); i++) {
//                            prefGames.add(task.getResult().child(data.get(i).getId()).getValue(CollectionSettings.class));
//                        }
//                        Log.d("MMMMMM2.2", "2.2 zaooo");
//                        ///get prefs for others
//                        prefOther.add(task.getResult().child(Constantes.ID_NOTIFICATION).getValue(CollectionSettings.class));
//                        Log.d("MMMMMM3", "Get Data from db");
//                    }
//                    ///Serialize the list for use later
//                    Serializer.serialize(Constantes.PREF_GAMES_LIST, prefGames, MainActivity.this);
//                    Serializer.serialize(Constantes.SETTINGS_LIST, prefOther, MainActivity.this);
//                    ///initialize SharedPreference for selection in settings
//                    ///Key = id, value = boolean
//                    SharedPreferences sharedpreferences = getSharedPreferences(Constantes.SHARED_PREFS, (MainActivity.this).MODE_PRIVATE);
//                    SharedPreferences.Editor editor = sharedpreferences.edit();
//                    for (int i = 0; i < prefGames.size(); i++) {
//                        editor.putString(prefGames.get(i).getId(), prefGames.get(i).getValue().toString());
//                    }
//                    for (int i = 0; i < prefOther.size(); i++) {
//                        editor.putString(prefOther.get(i).getId(), prefOther.get(i).getValue().toString());
//                    }
//                    // to save our data with key and value.
//                    editor.apply();
//                    Log.d("MMMMMM4", String.valueOf(task.getResult().getValue()));
                    progressDialog.dismiss();
                    getSupportFragmentManager().beginTransaction().replace((R.id.fragment_container), new SettingFragment()).commit();
                    toolbar.setTitle("Paramètres");
//                }
//            }
//        });
    }
}