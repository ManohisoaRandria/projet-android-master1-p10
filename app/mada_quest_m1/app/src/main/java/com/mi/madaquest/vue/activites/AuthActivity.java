package com.mi.madaquest.vue.activites;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mi.madaquest.R;
import com.mi.madaquest.interfaces.CallbackFragment;
import com.mi.madaquest.model.CheckNetwork;
import com.mi.madaquest.model.Constantes;
import com.mi.madaquest.utils.Serializer;
import com.mi.madaquest.vue.fragments.LoginFragment;
import com.mi.madaquest.vue.fragments.RegisterFragment;

import java.util.ArrayList;
import java.util.Map;

import static android.content.ContentValues.TAG;

public class AuthActivity extends AppCompatActivity implements CallbackFragment {

    Fragment fragment;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ///Pour masquer l'action bar
        getSupportActionBar().hide();
        setContentView(R.layout.activity_auth);

        if (!CheckNetwork.checknetwork(this)) {
            addFragment();
            Toast.makeText(this, "Please Check your Internet Connection.", Toast.LENGTH_SHORT).show();
        } else {
            if (FirebaseAuth.getInstance().getCurrentUser() != null) {
                ///User already logged in
                //Toast.makeText(AuthActivity.this, "Already logged in.", Toast.LENGTH_SHORT).show();
                goToMainHome();
            } else {
                //makany am login satria tsy mbola misy session
                addFragment();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        //Save the fragment's instance
        //getSupportFragmentManager().putFragment(outState, "FragmentM", this.fragment);
    }

    /**
     * Pour ajouter/afficher le fragment (login) à la vue
     */
    private void addFragment() {
        LoginFragment fragment = new LoginFragment();
        fragment.setCallbackFragment(this);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add((R.id.fragmentContainerAuth), fragment);
        fragmentTransaction.commit();
        this.fragment = fragment;
    }

    /**
     * Recoit en paramètre l'instance du fragment auquel on veut afficher
     *
     * @param frag
     */
    private void replaceFragment(Fragment frag) {
        if (frag instanceof LoginFragment) {
            LoginFragment fragment = (LoginFragment) frag;
            fragment.setCallbackFragment(this);
            this.fragment = fragment;
        } else if (frag instanceof RegisterFragment) {
            RegisterFragment fragment = (RegisterFragment) frag;
            fragment.setCallbackFragment(this);
            this.fragment = fragment;
        }
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.replace((R.id.fragmentContainerAuth), this.fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void changeFragment(Fragment fragment) {
        replaceFragment(fragment);
    }

    @Override
    public Context getContext() {
        return AuthActivity.this;
    }

    @Override
    public void goToMainHome() {

        Intent intent = new Intent(AuthActivity.this, MainActivity.class);
        //Ferme l'activity en cours avant d'en ouvrir une autre
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        //Call this to finish the current activity
        finish();
    }

}