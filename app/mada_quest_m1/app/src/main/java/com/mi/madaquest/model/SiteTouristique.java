package com.mi.madaquest.model;

public class SiteTouristique {
    private String id;
     private String idVillle;
     private String nom;
     private String description;
     private String contact;
     private String mail;
     private String siteweb;
     private double prixdevisite;
     private String ouvert;
     private String fermer;
     private double lat;
     private double lng;
     private String etat;
     private String nomville;
     private String nomzone;
     private String recherche;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
