package com.mi.madaquest.vue.activites;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.mi.madaquest.R;

public class SplashScreenActivity extends AppCompatActivity {
    ///Splash screen duration
    private final static int SPLASH_SCREEN = 4000;

    ///Variables
    Animation topAnim;
    ImageView im;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ///Pour masquer le title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ///Pour masquer l'action bar
        getSupportActionBar().hide();

        setContentView(R.layout.splash_screen_main);

        ///Animations
        topAnim = AnimationUtils.loadAnimation(this,R.anim.top_animation);
        im = findViewById(R.id.splash_Screen);
        im.setAnimation(topAnim);

        ///Passer à l'activité suivante
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreenActivity.this, AuthActivity.class);
                //Ferme l'activity en cours avant d'en ouvrir une autre
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                //Call this to finish the current activity
                finish();
            }
        },SPLASH_SCREEN);

    }
}