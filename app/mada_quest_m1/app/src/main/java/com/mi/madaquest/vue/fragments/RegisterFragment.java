package com.mi.madaquest.vue.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.mi.madaquest.R;
import com.mi.madaquest.interfaces.CallbackFragment;
import com.mi.madaquest.model.Constantes;
import com.mi.madaquest.model.MySingleton;
import com.mi.madaquest.vue.activites.MainActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class RegisterFragment extends Fragment {

    CallbackFragment callbackFragment;
    TextView connectIntoAccount;
    EditText inputRegisterEmail;
    EditText inputRegisterPassword;
    EditText inputRegisterPasswordConfirm;
    EditText inputRegisterPseudo;
    Button registerBtn;
    ProgressDialog progressDialog;

    FirebaseAuth mAuth;

    ImageView btnRegisterTwitter;
    ImageView btnRegisterFacebook;
    ImageView btnRegisterGoogle;

    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_register, container, false);
        this.inputRegisterEmail = rootView.findViewById(R.id.inputRegisterEmail);
        this.inputRegisterPassword = rootView.findViewById(R.id.inputRegisterPassword);
        this.inputRegisterPasswordConfirm = rootView.findViewById(R.id.inputRegisterPasswordConfirm);
        this.registerBtn = rootView.findViewById(R.id.registerBtn);
        this.connectIntoAccount = rootView.findViewById(R.id.connectIntoAccount);
        this.inputRegisterPseudo = rootView.findViewById(R.id.inputRegisterPseudo);
        this.btnRegisterTwitter = rootView.findViewById(R.id.btnRegisterTwitter);
        this.btnRegisterFacebook = rootView.findViewById(R.id.btnRegisterFacebook);
        this.btnRegisterGoogle = rootView.findViewById(R.id.btnRegisterGoogle);

        progressDialog=new ProgressDialog(rootView.getContext());
        mAuth=FirebaseAuth.getInstance();

        this.connectIntoAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(callbackFragment != null) {
                    callbackFragment.changeFragment(new LoginFragment());
                }
            }
        });

        this.registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PerformAuth();
            }
        });

        this.btnRegisterGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(callbackFragment.getContext(), GoogleSignInActivity.class);
//                startActivity(intent);
                Toast.makeText(callbackFragment.getContext(), "Fonctionalité en cours de dev", Toast.LENGTH_SHORT).show();
            }
        });

        this.btnRegisterFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(callbackFragment.getContext(), FacebookAuthActivity.class);
//                startActivity(intent);
                Toast.makeText(callbackFragment.getContext(), "Fonctionalité en cours de dev", Toast.LENGTH_SHORT).show();
            }
        });

        this.btnRegisterTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(callbackFragment.getContext(), TwitterActivity.class);
//                startActivity(intent);
                Toast.makeText(callbackFragment.getContext(), "Fonctionalité en cours de dev", Toast.LENGTH_SHORT).show();
            }
        });

        return rootView;
    }

    public void setCallbackFragment(CallbackFragment callbackFragment) {
        this.callbackFragment = callbackFragment;
    }

    private void PerformAuth() {
        String email = inputRegisterEmail.getText().toString();
        String password = inputRegisterPassword.getText().toString();
        String confirmPassword = inputRegisterPasswordConfirm.getText().toString();
        String pseudo = inputRegisterPseudo.getText().toString();

        if(pseudo.isEmpty() || pseudo.length()<4) {
            inputRegisterPseudo.setError("Please enter a valid Pseudo");
        }else if(!email.matches(Constantes.REGEX_EMAIL)) {
            inputRegisterEmail.setError("Please enter a valid Email");
        }else if(password.isEmpty() || password.length()<6) {
            inputRegisterPassword.setError("Please enter a proper Password (6 characters min)");
        }else if(!password.equals(confirmPassword)) {
            inputRegisterPasswordConfirm.setError("The Password does not match");
        }else {
            progressDialog.setMessage("euillez patienter...");
            progressDialog.setTitle("Inscription");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

            mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    //progressDialog.dismiss();
                    if(task.isSuccessful()) {
                        FirebaseUser user = mAuth.getCurrentUser();
                        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                .setDisplayName(pseudo)
                                .build();

                        user.updateProfile(profileUpdates)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            //insert user to database
//                                            User user = new User(email,pseudo);
                                            postDataUsingVolley(email,pseudo);

                                        }else{
                                            progressDialog.dismiss();
                                            Toast.makeText(callbackFragment.getContext(),""+task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    }else {
                        progressDialog.dismiss();
                        Toast.makeText(callbackFragment.getContext(),""+task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
    private void postDataUsingVolley(String email, String pseudo) {
        // url to post our data
            String url =Constantes.API_URL+"/users/insertUsers";

            // creating a new variable for our request queue
            MySingleton singleton = MySingleton.getInstance(requireContext());

        // on below line we are calling a string
        // request method to post the data to our API
        // in this we are calling a post method.
        StringRequest request = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                // inside on response method we are
                // hiding our progress bar
                // and setting data to edit text as empty

                // on below line we are displaying a success toast message.
                progressDialog.dismiss();
                inputRegisterEmail.setText("");
                inputRegisterPasswordConfirm.setText("");
                inputRegisterPassword.setText("");
                inputRegisterPseudo.setText("");
                Toast.makeText(callbackFragment.getContext(),"Registration Successful",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(callbackFragment.getContext(), MainActivity.class);
                //Ferme l'activity en cours avant d'en ouvrir une autre
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mAuth.getCurrentUser().delete()
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    progressDialog.dismiss();
                                    Toast.makeText(callbackFragment.getContext(),""+error.getMessage(),Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                // below line we are creating a map for
                // storing our values in key and value pair.
                Map<String, String> params = new HashMap<>();

                // on below line we are passing our key
                // and value pair to our parameters.
                params.put("nom", pseudo);
                params.put("prenom", pseudo);
                params.put("mail", email);
                params.put("id", "");
                params.put("motDePasse", "");
                params.put("contact", "+216342585789");
                params.put("etat", "0");


                // at last we are
                // returning our params.
                return params;
            }
        };
        // below line is to make
        // a json object request.
        singleton.addToRequestQueue(request);
    }
    private void sendVerificationEmail() {
        mAuth.getCurrentUser().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    // email sent
                    // after email is sent just logout the user and finish this activity
                    Toast.makeText(callbackFragment.getContext(),"A Validation Email has been sent to "+mAuth.getCurrentUser().getEmail()+".",Toast.LENGTH_SHORT).show();
                    mAuth.signOut();
                    //connectIntoAccount.performClick();
                }else {
                    // email not sent, so display message and restart the activity or do whatever you wish to do
                    //restart this activity
                    Toast.makeText(callbackFragment.getContext(),"An Error occurred while sending the Validation Email.",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        if(mAuth.getCurrentUser() != null) {
            //handle the already log in user
        }
    }
}